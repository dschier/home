# Architecture

This document describes the general architecture for my home network.

## User demand

Since I am my own user, I came up with the below demand.

### Must

- I want to deploy everything as container
- I want to have my "normal" workload rock solid running
- I want a Lab to play with Kubernetes, including worker scenarios
- I want to have everything in code that is meant to be "production"
- I want to keep default OS security settings
- I want to administrate everything from the command line

### Should

- I desire automatic updates
- I desire graphical representations of my environment
- I desire graphical administration options
- I desire to use the standard LAN functionalities (no special hardware)
- I desire to document everything thoroughly
- I desire to keep the repository updated

### May

- I wish for automatic certificate handling for my domain
- I wish for testing capabilities for the while-true-do.io environment

## General Concepts

### Network

### Storage

### Compute

### Lab

## Risk Assessment
