# Architecture

This document describes some architectural decisions for the lab machines.

## Operating Systems

As the underlying operating system, I have chosen AlmaLinux OS. It keeps the
cognitive load low (everything running on RH family machines), has some nice
features for the Raspberry Pi images and my preferred Kubernetes distribution
is running on it.

## Services

As the bare minimum services on this machine, I have chose the default RH-family
setup plus Cockpit for web management. In total, this boils down to:

- Cockpit provides a nice web interface for management
- SSH for remote management via Shell
- Tuned for power profiling
- Performance Co-Pilot for metrics tracking
- Firewalld and SELinux enabled for additional security
- OpenSCAP CIS profiles for hardening

## Container

The lab machines are meant to run container workload only. Therefore, it was
somewhat natural to use Kubernetes. I want to experiment, play in a sandbox and
destory/clean up enture namespaces, without taking care of too much. Also, I am
eager to play with high-availability scenarios, canary deployments, A/B-Tests
and much more.

### Engine

To run my containers, I have chosen Kubernetes. Due to its small footprint,
nice out-of-the-box features and easy setup, I have chose k3s as the Kubernetes
distribution.

### Storage

When it comes to storage, I have multiple options. For the lab setup, I am
focusing on cloud native development mostly. Therefore, an S3 compatible storage
is mandatory. I opted to use Minio for the same. It provides some nice features,
has a rather small footprint and is easy to maintain.

In case something comes along that really needs filesystem storage, I still can
publish some NFS share from my storage solution.

### Deployments

When it comes to deployments, I want to have everything in Kubernetes manifests,
yet limit the amount of tools. The following options are considered, in this
particular order.

1. Ansible as the orchestrator
2. Kustomize for everything, I want to use internally
3. Helm, in case the vendor ships something, or I want ti publish a package
4. Kubernetes Manifests for demonstrational purposes or quick testing
5. Kubernetes Operators, if there is really no other way

Similar to the [compute](./ARCHITECTURE_COMPUTE.md) deployments, I want to
limit the amount of exposed ports and therefore push everything through an
Ingress. Fortunately, k3s comes with Traefik preinstalled.

## Operate

Operating Kubernetes is a totally different story than handling good 'ol package
updates. In fact, we do have a cluster that needs to be on the same version,
hopefully hard coded deployments and still an underlying OS.

### Update

For updates, I consider four major parts, as described below. To orchestrate
these updates, I will consider doing these manually for now, but check out if
one can orchestrate the operation nicely.

#### Deployment Updates

If a deployment needs to be updated, you can change the Kubernetes manifest
and push it. There is (good) auto-update mechanism available to do this for
you. Instead, one might use tools like Flux-CD or ArgoCD for automated updates.
Still, this is considered "bad practice" in production.

#### Storage Updates

Updating the storage is another bigger topic. Minio itself provides update
mechanisms, but one needs to take care of these and there might be hiccups
when it comes to these updates.

#### Kubernetes Updates

The Kubernetes distribution itself gets updates, too. To apply these, one needs
to drain workload, take care of the order of deployments and even face downtimes
when the controller is not HA.

#### OS Updates

Lastly, the underlying OS is also getting updates for the Kernel, some packages,
libraries, etc. We need to update these and also reboot the desired machine
afterwards. Draining the workload beforehand might be a good idea.

### Backup

When it comes to backups, one has two consider two major parts. Both provide
nice backup mechanisms, but need to be used properly.

#### ETCD Backups

ETCD holds the Kubernetes cluster data. In the database, you will have all
information about the current state and which deployments are running where.
ETCD provides snapshot and backup tools, so I don't see too much hastly here.

Also, I might just re-instantiate the whole cluster if it is broken.

#### Minio

For the persistent volumes and object storages, the story is a bit different.
Most likely some of the data is not available in code. Fortunately, there are
some really nice tools to backup S3 storage objects. One of them is Restic,
that ended up on my short list.

Still, for now the lab will only hold some data that can be thrown away and
is not considered important.
