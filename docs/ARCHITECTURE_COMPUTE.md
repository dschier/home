# Architecture

This document describes some architectural decisions for the compute machines.

## Diagram

The below diagram should provide a quick overview and some context about the
following sections and architectural decisions.

![architecture compute](./assets/architecture_compute.drawio.svg)

## Operating System

As the operating system, I have chosen AlmaLinux OS. This decision was carefully
made by evaluating the demand and "harmony" in my network. The following aspects
were taken into considerations.

- vanilla container support with Podman
- Firewalld provides good firewall services
- SELinux adds improved security
- Systemd makes it easy to manage Podman containers and services
- AlmaLinux is a stabilized variant of CentOS Stream and has nice community
  support, but also enterprise influences
- OpenSCAP integration provides compliance profiles

## Services

When it comes to services, I have chose a couple that make the life easier, but
also allow a comfortable usage of the system.

- Cockpit provides a nice web interface for management
- Podman is well integrated
- SSH for remote management via Shell
- Tuned for power profiling
- Performance Co-Pilot for metrics tracking
- Firewalld and SELinux enabled for additional security
- OpenSCAP CIS profiles for hardening

## Container

I want to deploy all workload into containers. This allows to make them easy to
replace, update, upgrade and destroy. The combination of systemd. Podman,
Cockpit and firewalld works pretty solid, too.

### Engine

Podman is the container engine of choice. It can be considered a Docker drop-in
replacement, can run rootless workloads and has good integration in systemd.
Podman Quadlets, Cockpit Podman and others make it a breeze to work with
Containers, container storage or networks.

### Storage

On the compute machine, storage will be on a dedicated disk. The mountpoint
`/var/lib/containers` holds not only images, but also persistent, named storage
volumes.

### Deployments

The following diagram should showcase the default idea of a deployment and how
it is made from different components, each defined in a Podman Quadlet file.

![container deployment](./assets/architecture_container_deployment.drawio.svg)

The definition of components like volumes, networks or containers should be
entirely made in Podman Quadlets. In addition, I will use Ansible to generate
these quadlet files.

## Operate

We have to consider some operational tasks in our architecture. This section
describes these operation considerations.

### Update

Updating an application or system can be cumbersome. The more software is
installed in the operating system context itself, the more error-prone might be
the update.

Due to the split of system and application with containers, this task becomes
easier. In a standard scenario, you can update each application individually,
even automatic and handle the OS in a separate task.

For the home server scenario, I consider updating both automatically. Podman
has a service to handle auto-updates, based on image versions. AlmaLinux OS
provides an auto-update mechanism with `dnf-automatic`.

### Backup

Backing up the system and its services boils down to some simple steps. For
the system itself, no backup is needed. Everything should be reproducible with
this repository.

For the data, generated in containers, one needs to copy the volumes content
to a desired backup destination. This can be done in many ways, but a "pull-
approach" is recommended.
