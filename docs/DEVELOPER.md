# Developer

This document describes the development standards and documents how you can
adapt the repository or contribute.

## Guidelines

### Ansible

### Cloud Init

### Kickstart

### Kustomize

## Documentation

## Test

## Continuous Integration

## Continuous Delivery

## Continuous Deployment
