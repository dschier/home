# User

This document describes how one can use the provided services.

## Cockpit (Server Management)

Cockpit is a web interface for server management. It is mostly meant for
administrative work. But, it might provide some useful insights for users, too.

Please consult the [Admin documentation](./ADMIN.md) to get an introduction
about Cockpit.

## SSH (Server Management)

## Podman

## Ingress/Proxy Dashboard

- Traefik
