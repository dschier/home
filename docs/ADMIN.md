# Admin

This document describes the initial setup, configuration and operational tasks
one may need to perform to install the solution or maintain it on a regular
basis.

## Architecture

Before administration, I strongly suggest to take a look at the
[Architecture documentation](./ARCHITECTURE.md), first.

## Requirements

For the initial setup, the best option would be some kind of install server.
This can be something fully fledged like [MAAS](https://maas.io/) or
[Foreman](https://theforeman.org/). You can also build your own thing with
[Cobbler](https://cobbler.github.io/), PXE, etc.

For the sake of a home setup, such a sophisticated setup may be out of scope. At
least it is for the sake of this documentation. Instead, let's assume that you
have at least one machine running in your local are network with Fedora,
AlmaLinux or another Red Hat derivate. This machine might even be your own
workstation.

For the rest of this documentation this machine will be referenced as
**install-node**. Below, you can find the steps to prepare said
**install-node**.

### Get the code

To get the code, it is recommended to use Git. This allows to work with the
code in this repository.

```shell
# Install Git
$ sudo dnf install git-core

# Check out the repository
$ git clone https://gitlab.com/dschier/home.git
```

### Install Ansible

Next, you should install Ansible and the required collections. This makes your
**install-node** an Ansible control node effectively.

```shell
# Install Ansible (from distribution package)
$ sudo dnf install ansible-core

# Install ansible collections
$ ansible collection install -r ansible/collections/requirements.yml
```

### Python

Python3 should be there already. We will use it later on to start a makeshift
web server and serve some files.

```shell
# Ensure python is there
$ python --version
Python 3.12.1

# Ensure python web server can be started
$ python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

## Initial Setup

In a home network scenario, the initial setup is done occasionally, only.
Therefore, I haven't fully automated the process and it requires a little bit
of manual work.

### Provision Compute Machines

For provisioning of the compute machine, you need a web server, that serves the
kickstart files. Also, you will need a USB thumb drive with AlmaLinux OS 9. The
desired compute machine needs to be connected to the internet, so it can
download packages during installation.

Therefore, your setup may look something like this:

![kickstart setup]()

For the next steps, I will refer to **install-node** as the node holding the
kickstart files (as described above) and **compute-node** for the machine that
will become our compute device.

1.
2.
3.
4.
5.

### Provision Lab Machines

Provisioning the lab machines is simpler than you might think, initially. We can
make use of Cloud-Init, which simplifies the process tremendously. The process
boils down to:

1. Download the AlmaLinux OS 9 Raspberry Image from
   [here](https://almalinux.org/get-almalinux/#Raspberry_Pi-2)
2. Write it to a USB device or SD Card with either:
   a) `dd`
   b) Raspberry Pi Imager
   c) Balena Etcher
3. Copy the additional files from the `cloud-init` directory to the `CIDATA`
   partition (Edit them to your liking beforehand)
4. Plug in your device to a Raspberry Pi (Either via USB or SD Card)
5. Boot Up the Raspberry Pi and wait patiently
6. Check if cloud-init is done via `/var/log/cloud-init-output.log`

All the basic packages will be installed for you, a user will be created and
you can continue with the next steps.

### Initial Configuration

- Ansible

## Container Deployments

- ansible
- podman (quadlets)
- podman remote
- Ingress

## Maintenance

### Requirements

- ansible

### Maintenance Tools

- cockpit
- ansible
- podman remote

### Updates

- podman auto-update
- automatic system update
- manual system updates

### Housekeeping

- podman clean-up

## Backup and Recovery

- system backups
- podman backups
