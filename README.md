# home

My personal home server and lab setup.

## Disclaimer

This repository is meant for my personal use. You may look up things and even
fork it, or tune it to your liking. But, there is no guarantee, that it may
work for you, too.

In case you want to reproduce a similar setup, please have a look at the
underlying automation, made available in the
[while-true-do.io project](https://gitlab.com/whiletruedoio).

## Motivation

As an enthusiast, I want to run some services on my own for my home, but also
experiment with new technologies, HA setups and more for personal education. To
achieve the same, I require some setup, that is easy to reproduce and flexible
enough for adoption.

## History

In the past, I had a Fedora Home Server, which I already tackled in some
[blog articles](https://blog.while-true-do.io/fedora-home-server-intro-concept/).
Well, after some years, I did switch to a different OS, moved things around, and
now the setup is bigger, has more moving parts, etc.

Instead of having one repository per server, I opted to put my entire home
environment in this repository.

## Description

My home setup is not really complicated, but is also not a single machine. In
general, it is made out of Linux machines, installed with AlmaLinux OS and
some network and storage to connect everything.

### Features

My home setup, as it is automated here, has some nifty features, so I am mostly
using it and not just doing maintenance.

- strongly built around containers
- administration via SSH, WebUI and other tools
- lots of security considerations
- self-updating
- regular backups
- no special hardware needed

### Content

The following content, to achieve the above, is maintained in the repository:

- kickstart and cloud-init files for initial setup
- ansible playbooks for continuous configuration management and changes
- other helpers to make machines working
- documentation to explain how everything is tied together
- additional scripts, links and configuration, if necessary

## Usage

The code in this repository provides a setup, which is documented for users in
the [User documentation](./docs/USER.md) and for administrators/operators of
the setup in the [Admin documentation](./docs/ADMIN.md). The detailed
architecture can be found in the
[architecture documentation](./docs/ARCHITECTURE.md).

## Contribute

Thank you so much for considering to contribute. Please don't hesitate to
provide feedback, report bugs, request features or contribute code. Please
make yourself comfortable with the
[contributing guidelines](https://gitlab.com/whiletruedoio/gitlab-profile/-/blob/main/docs/CONTRIBUTING.md)
first.

### Issues

Opening issues and reporting bugs is pretty easy. Please feel free to open a
report via the issue tracker or send an e-mail.

- [Issue Tracker](https://gitlab.com/whiletruedoio/kickstart-files/-/issues)
- [Support Mail](mailto:support@while-true-do.io)

### Development

In case you consider to contribute with your development, please check out the
[development documentation](./docs/DEVELOPMENT.md).

## License

Except otherwise noted, all work is [licensed](LICENSE) under a
[BSD-3-Clause License](https://opensource.org/licenses/BSD-3-Clause).

## Contact

In case you want to get in touch with me or reach out for general questions,
please use one of the below contact details

- Blog: [blog.while-true-do.io](https://while-true-do.io)
- Code: [gitlab.com/whiletruedoio](https://gitlab.com/whiletruedoio)
- Chat: [#whiletruedoio-community](https://matrix.to/#/#whiletruedoio-community:matrix.org)
- Mail: [hello@while-true-do.io](mailto:hello@while-true-do.io)
